package br.com.mastertech.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, Acesso> producer;

    public void enviarAoKafka(Acesso acesso) {

            producer.send("spec2-yuri-andrei-1", acesso);

    }
}