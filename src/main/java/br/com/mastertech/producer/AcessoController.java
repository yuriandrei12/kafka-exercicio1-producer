package br.com.mastertech.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

@RestController
public class AcessoController {

    @Autowired
    private AcessoProducer acessoProducer;

    @GetMapping("/acesso/{cliente}/{porta}")
    public void acesso(@PathVariable String nome, @PathVariable String portaAcesso) {
        Acesso acesso = new Acesso();
        acesso.setNome(nome);
        acesso.setPortaAcesso(portaAcesso);
        acesso.setTemAcesso(new Random().nextBoolean());
        acessoProducer.enviarAoKafka(acesso);
    }
}
